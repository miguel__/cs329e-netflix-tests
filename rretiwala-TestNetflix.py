#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----


    def test_eval_1(self):
        r = StringIO("2854:\n620010\n")
        w = StringIO()
        netflix_eval(r,w)
        self.assertEqual(
            w.getvalue(), "2854:\n2.957\n0.95\n")

    def test_eval_2(self):
        r = StringIO("1001:\n1050889\n67976\n1025642\n624334\n")
        w = StringIO()
        netflix_eval(r,w)
        self.assertEqual(
            w.getvalue(), "1001:\n3.4185\n3.5775\n3.7504999999999997\n3.324\n0.73\n")

    def test_eval_3(self):
        r = StringIO("2846:\n884594\n321050\n151732\n")
        w = StringIO()
        netflix_eval(r,w)
        self.assertEqual(
            w.getvalue(), "2846:\n2.439\n2.8415\n2.751\n0.51\n")


    def test_eval_4(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n3.2874999999999996\n3.4605\n3.83\n0.94\n")




# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
